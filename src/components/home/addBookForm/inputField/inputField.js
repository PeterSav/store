import React from "react";
import "./inputField.scss";
import { connect } from "react-redux";
import { handleAddBookInputs } from "../../../../models/actions";

const InputField = ({
    fieldName,
    dispatch,
    placeHolder,
    bookToAdd,
    bookToAddErrors,
    showErrors,
}) => {
    const handleInputChange = e => {
        dispatch(handleAddBookInputs(fieldName, e.target.value));
    };

    return (
        <div className="inputField__wrapper">
            <label htmlFor={fieldName}>{placeHolder}: </label>
            <input
                className="inputField__field"
                type="text"
                name={fieldName}
                id={fieldName}
                style={
                    showErrors
                        ? bookToAddErrors[fieldName]
                            ? { borderColor: "red" }
                            : undefined
                        : undefined
                }
                placeholder={placeHolder}
                value={bookToAdd[fieldName]}
                onChange={handleInputChange}
            />
        </div>
    );
};

const mapStateToProps = state => {
    return {
        bookToAdd: state.bookToAdd,
        bookToAddErrors: state.bookToAddErrors,
        showErrors: state.showErrors,
    };
};

export default connect(mapStateToProps)(InputField);
