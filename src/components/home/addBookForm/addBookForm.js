import React, { useEffect } from "react";
import "./addBookForm.scss";
import { connect } from "react-redux";
import {
    handleAddBookInputs,
    newBookInitialize,
    addBookButtonPressed,
} from "../../../models/actions";
import { Link } from "react-router-dom";

import InputField from "./inputField/index";

const AddBookForm = ({
    dispatch,
    bookToAdd,
    showErrors,
    bookToAddErrors,
    showSaveBookButton,
    idOfNewBook,
}) => {
    const { description } = bookToAdd;

    const handleInputChange = e => {
        dispatch(handleAddBookInputs("description", e.target.value));
    };

    const addBookHandler = () => {
        dispatch(addBookButtonPressed());
    };

    useEffect(() => {
        dispatch(newBookInitialize());
    }, []);

    return (
        <div>
            <h2 className="addBook__h2">Add new Book</h2>
            <div className="addBook__container">
                <form className="addBook__form">
                    <section className="addBook__form__column">
                        <InputField fieldName="title" placeHolder="Title" />
                        <div className="inputField__wrapper">
                            <label htmlFor="description">Description: </label>
                            <textarea
                                className="inputField__field"
                                name="description"
                                id="description"
                                placeholder="Description"
                                rows="1"
                                style={
                                    showErrors
                                        ? bookToAddErrors.description
                                            ? { borderColor: "red" }
                                            : undefined
                                        : undefined
                                }
                                value={description}
                                onChange={handleInputChange}
                            ></textarea>
                        </div>
                        <InputField
                            fieldName="categories"
                            placeHolder="Categories"
                        />
                        <InputField
                            fieldName="author"
                            placeHolder="Author Names"
                        />
                        <InputField
                            fieldName="publisher"
                            placeHolder="Publisher"
                        />
                        <InputField fieldName="year" placeHolder="Year" />
                        <InputField
                            fieldName="pages"
                            placeHolder="Pages Number"
                        />
                    </section>
                    <section className="addBook__form__column">
                        <InputField fieldName="isbn10" placeHolder="ISBN-10" />
                        <InputField fieldName="isbn" placeHolder="ISBN-13" />
                    </section>
                </form>
                <div className="addBook__button__container">
                    <div className="addBook__buttons__flex">
                        <button
                            className="addBook__button"
                            onClick={addBookHandler}
                        >
                            add Book
                        </button>
                        {showSaveBookButton && (
                            <Link
                                className="addBook__seeButton"
                                to={`/books/${idOfNewBook}`}
                            >
                                SEE BOOK
                            </Link>
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        bookToAdd: state.bookToAdd,
        bookToAddErrors: state.bookToAddErrors,
        showErrors: state.showErrors,
        showSaveBookButton: state.showSaveBookButton,
        idOfNewBook: state.idOfNewBook,
    };
};

export default connect(mapStateToProps)(AddBookForm);
