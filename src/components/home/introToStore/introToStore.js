import React from "react";
import "./introToStore.scss";

const IntroToStore = () => (
    <>
        <h1 className="intro__h1">
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
        </h1>
        <br />
        <p className="intro__text">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Pariatur
            qui quia eum temporibus odit? Ipsa eum officia, molestiae officiis
            magnam ratione iste tempora hic natus vel nihil ex et nam.Fuga unde
            reiciendis dolorum iusto et quod eaque recusandae vel similique
            laboriosam quasi dolor, nemo, veniam ad sequi commodi reprehenderit
            tempore quidem qui odio? Iste sed provident consequatur rerum
            expedita.
        </p>
        <p className="intro__text">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem a
            obcaecati ut, provident facilis tempore ea esse aliquid similique
            sunt, fugit velit quos et nobis consequuntur illo error inventore
            ad. Eos exercitationem, beatae non illum voluptatem nisi ea. Ipsam
            adipisci molestias praesentium necessitatibus in, doloribus sunt
            dolor modi aliquid tempora eos fugit totam ea ad assumenda voluptate
            quasi illum iure? Consequuntur fugit minima natus modi praesentium
            eos ab expedita, ratione quia sed tempora tenetur facilis pariatur
            qui veritatis odio perferendis eius esse voluptate deserunt tempore?
            Fuga suscipit officia voluptatum eius! Mollitia dignissimos ad
            laboriosam at culpa tempore facere recusandae, hic iusto reiciendis
            quidem deleniti non dolor a alias, impedit doloremque nisi vero
            harum tenetur vitae esse! Hic voluptates animi repellendus!
            Voluptate laudantium provident maiores minus suscipit doloribus.
            Tenetur voluptatum officia necessitatibus maxime a harum. Excepturi
            nobis reiciendis consectetur quaerat natus et dolore doloribus enim
            fugiat. Molestiae ea corporis similique possimus.
        </p>
        <p className="intro__text">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid
            laudantium soluta pariatur sequi numquam debitis nisi maxime
            blanditiis. Cumque quaerat architecto qui ducimus minima eos
            consequuntur exercitationem culpa eaque quam? Dignissimos ut ea
            deleniti quasi! Rerum, illum. Nam, facere! Illum quam ipsam vero
            officiis soluta nesciunt tempore aspernatur hic suscipit, iste
            libero natus nemo, nam esse fugiat sed autem ullam? Optio fugit
            obcaecati, maxime asperiores libero quas hic cum. Enim quis numquam
            provident harum obcaecati reprehenderit debitis voluptate, magnam
            voluptatum deleniti hic veniam et officia iusto mollitia quae rem
            laudantium! Provident laudantium nobis esse illo voluptate beatae
            consequatur repellat dolorum quod quidem cum obcaecati nesciunt
            accusamus, ducimus libero ipsum incidunt! Consequatur quidem vel
            iste modi possimus, temporibus minus voluptates. Tempora. Minima
            autem, ea odit ex quisquam quas non incidunt consequuntur harum
            deleniti sed distinctio eius veritatis fugiat expedita consequatur
            tempore excepturi amet tenetur officiis officia? Blanditiis a
            repudiandae illum sint. Sit culpa est tempore officia, odit quasi
            eius quidem quaerat velit ad aut voluptatum modi quia facere ratione
            distinctio, doloremque repellat consequuntur consequatur. Itaque
            aliquam voluptatum dolorum voluptate suscipit ab! Quae asperiores
            deleniti, consequatur nesciunt assumenda ea eveniet necessitatibus
            ex voluptatum deserunt autem illum corporis! Nulla nobis blanditiis
            dignissimos sunt corrupti, magnam omnis incidunt, ullam earum error
            dolores excepturi maiores.
        </p>
        <p className="intro__text">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquid
            laudantium soluta pariatur sequi numquam debitis nisi maxime
            blanditiis. Cumque quaerat architecto qui ducimus minima eos
            consequuntur exercitationem culpa eaque quam? Dignissimos ut ea
            deleniti quasi! Rerum, illum. Nam, facere! Illum quam ipsam vero
            officiis soluta nesciunt tempore aspernatur hic suscipit, iste
            libero natus nemo, nam esse fugiat sed autem ullam? Optio fugit
            obcaecati, maxime asperiores libero quas hic cum. Enim quis numquam
            provident harum obcaecati reprehenderit debitis voluptate, magnam
            voluptatum deleniti hic veniam et officia iusto mollitia quae rem
            laudantium! Provident laudantium nobis esse illo voluptate beatae
            consequatur repellat dolorum quod quidem cum obcaecati nesciunt
            accusamus, ducimus libero ipsum incidunt! Consequatur quidem vel
            iste modi possimus, temporibus minus voluptates. Tempora. Minima
            autem, ea odit ex quisquam quas non incidunt consequuntur harum
            deleniti sed distinctio eius veritatis fugiat expedita consequatur
            tempore excepturi amet tenetur officiis officia? Blanditiis a
            repudiandae illum sint. Sit culpa est tempore officia, odit quasi
            eius quidem quaerat velit ad aut voluptatum modi quia facere ratione
            distinctio, doloremque repellat consequuntur consequatur. Itaque
            aliquam voluptatum dolorum voluptate suscipit ab! Quae asperiores
            deleniti, consequatur nesciunt assumenda ea eveniet necessitatibus
            ex voluptatum deserunt autem illum corporis! Nulla nobis blanditiis
            dignissimos sunt corrupti, magnam omnis incidunt, ullam earum error
            dolores excepturi maiores.
        </p>
    </>
);

export default IntroToStore;
