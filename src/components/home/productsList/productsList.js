import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "./productsList.scss";
import { connect } from "react-redux";
import star from "../../../assets/star.svg";
import SearchBar from "./searchBar/index";
import cloneDeep from "lodash/cloneDeep";
import NavigationOfProducts from "./navigationOfProducts/index";

const ProductsList = ({ booksList, hasSearched, sortedList }) => {
    const booksPerPage = 6;

    const [pages, setPages] = useState(
        Math.floor(booksList.length / booksPerPage)
    );
    const [currentPage, setCurrentPage] = useState(1);

    const stars = [0, 1, 2, 3, 4];

    const starsOfBook = () =>
        stars.map(idx => (
            <img
                key={idx}
                src={star}
                alt="star svg"
                className="productsList__book__stars"
            />
        ));

    useEffect(() => {
        if (booksList.length % booksPerPage !== 0) {
            setPages(pages + 1);
        }
    }, []);

    const titleLengthFix = title => {
        if (title.length < 20) {
            return title;
        }
        let titleToReturn = title.split("");
        titleToReturn = titleToReturn.filter((title, id) => id < 16);
        titleToReturn.push("...");
        return titleToReturn.join("");
    };

    const renderBooks = () => {
        // ? if search has been used show sorted list. Else show the normal one
        const booksToMap = hasSearched
            ? cloneDeep(sortedList)
            : cloneDeep(booksList);
        let booksStartOfPage = currentPage * booksPerPage - booksPerPage;
        let booksEndOfPage = currentPage * booksPerPage;
        return booksToMap
            .filter((book, id) => id >= booksStartOfPage && id < booksEndOfPage)
            .map(({ title, img, id }) => {
                const titleToShow = titleLengthFix(title);
                return (
                    <div key={id} className="productsList__book">
                        <Link
                            to={`/books/${id}`}
                            className="productsList__book__link"
                        >
                            <figure>
                                <img
                                    src={img}
                                    alt="book Img"
                                    className="productsList__book__img"
                                />
                            </figure>
                            <figcaption>{titleToShow}</figcaption>
                        </Link>
                        <div>{starsOfBook()}</div>
                    </div>
                );
            });
    };

    return (
        <div>
            <SearchBar />
            <div className="productsList">{renderBooks()}</div>
            {pages > 1 && (
                <NavigationOfProducts
                    pages={pages}
                    currentPage={currentPage}
                    pageMinus={() => setCurrentPage(currentPage - 1)}
                    pagePlus={() => setCurrentPage(currentPage + 1)}
                />
            )}
        </div>
    );
};

const mapStateToProps = state => {
    return {
        booksList: state.booksList,
        hasSearched: state.search ? true : false,
        sortedList: state.sortedList,
    };
};

export default connect(mapStateToProps)(ProductsList);
