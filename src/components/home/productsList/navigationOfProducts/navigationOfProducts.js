import React from "react";
import "./navigationOfProducts.scss";
import leftChevron from "../../../../assets/leftChevron.svg";
import rightChevron from "../../../../assets/rightChevron.svg";

const NavigationOfProducts = ({ currentPage, pages, pagePlus, pageMinus }) => (
    <div className="navigation">
        <img
            className="navigation__chevron"
            src={leftChevron}
            onClick={pageMinus}
            style={currentPage === 1 ? { visibility: "hidden" } : undefined}
            alt="left chevron svg"
            width="24px"
            height="24px"
        />
        {currentPage} of {pages}
        <img
            className="navigation__chevron"
            src={rightChevron}
            onClick={pagePlus}
            style={currentPage === pages ? { visibility: "hidden" } : undefined}
            alt="right chevron svg"
            width="24px"
            height="24px"
        />
    </div>
);

export default NavigationOfProducts;
