import React, { useEffect } from "react";
import "./searchBar.scss";
import { searchInitialize } from "../../../../models/actions";
import { connect } from "react-redux";
import { handleSearchEpic } from "../../../../models/actions";

const SearchBar = ({ search, dispatch }) => {
    useEffect(() => {
        dispatch(searchInitialize());
    }, []);

    const handleSearch = e => {
        dispatch(handleSearchEpic(e.target.value));
    };

    return (
        <>
            <h2 className="searchBar__title">Search to find your new book</h2>
            <p className="searchBar__subtitle">
                Lorem ipsum, dolor sit amet consectetur adipisicing elit.
            </p>
            <input
                type="text"
                placeholder="Search..."
                name="search"
                value={search}
                onChange={handleSearch}
                className="searchBar__search"
            />
        </>
    );
};

const mapStateToProps = state => {
    return {
        search: state.search,
    };
};

export default connect(mapStateToProps)(SearchBar);
