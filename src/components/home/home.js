import React from "react";
import "./home.scss";
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ProductsList from "./productsList/index";
import IntroToStore from "./introToStore/index";
import Book from "./book/index";
import AddBookForm from "./addBookForm/index";

const Home = () => (
    <Router>
        <div>
            <header className="header">
                <Link to="/" className="header__logo">
                    logo
                </Link>
                <nav className="header__navigation">
                    <Link to="/books" className="header__navigation__links">
                        Books
                    </Link>
                    <Link
                        to="/addProduct"
                        className="header__navigation__links"
                    >
                        Add Book
                    </Link>
                </nav>
            </header>
            <Route exact path="/books/" component={ProductsList} />
            <Route exact path="/" component={IntroToStore} />
            <Route exact path={`/books/:id`} component={Book} />
            <Route path="/addProduct" component={AddBookForm} />
        </div>
    </Router>
);

const mapStateToProps = state => {
    return {
        applicationState: state.applicationState,
    };
};

export default connect(mapStateToProps)(Home);
