import React, { useEffect } from "react";
import "./book.scss";
import { connect } from "react-redux";
import RelevantBooksList from "./relevantBooksList/index";
import { createRelevanceList } from "../../../models/actions";

const Book = ({ match, booksList, dispatch }) => {
    const { id } = match.params;

    useEffect(() => {
        dispatch(createRelevanceList(booksList[id]));
    }, []);

    return (
        <div>
            <div className="book__container">
                <figure className="book__img__container">
                    <img
                        src={booksList[id].img}
                        alt="book img"
                        className="book__img"
                    />
                    {/* should have an svg here before the author */}
                    <figcaption> {booksList[id].author}</figcaption>
                    {/* stars would be rendered the same way they are in the productsList */}
                    <span>stars</span>
                </figure>
                <div className="book__content__container">
                    <h4 className="book__content__title">
                        {booksList[id].title}
                    </h4>
                    {/* we should have here a function that hides some of the content if it's very big and gives a see more/see less button in the end */}
                    <p className="book__content__description">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Nam fugiat, exercitationem, voluptatum, alias temporibus
                        ab in porro quae esse vero.
                    </p>
                    <div className="book__content__buttons">
                        <div className="book__content__button">favorite</div>
                        <div className="book__content__button">share</div>
                    </div>
                    <div className="book__category-year-pages">
                        <div>Category: {booksList[id].categories}</div>
                        <div>Year: {booksList[id].year}</div>
                        <div>Pages: {booksList[id].pages}</div>
                    </div>
                    <div className="book__publisher">
                        <div>Publisher: {booksList[id].publisher}</div>
                    </div>
                    <div className="book__isbn">
                        <div>ISBN-10: {booksList[id].isbn10}</div>
                        <div>ISBN-13: {booksList[id].isbn}</div>
                    </div>
                    <button className="book_buyButton">BUY</button>
                </div>
            </div>
            <RelevantBooksList />
        </div>
    );
};

const mapStateToProps = state => {
    return {
        booksList: state.booksList,
    };
};

export default connect(mapStateToProps)(Book);
