import React from "react";
import "./relevantBooksList.scss";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

const RelevantBooksList = ({ relevantToBookList }) => {
    const createRelevantList = () => {
        let books = relevantToBookList.slice(0, 4);
        return books.map(book => {
            return (
                <Link className="relevantList__book" to={`/books/${book.id}`}>
                    <img
                        src={book.img}
                        alt="book img"
                        className="relevantList__book__img"
                    />
                    <h5 className="relevantList__book__title">{book.title}</h5>
                </Link>
            );
        });
    };

    return (
        <div className="relevantList">
            <div className="relevantList__container">
                {createRelevantList()}
            </div>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        relevantToBookList: state.relevantToBookList,
    };
};

export default connect(mapStateToProps)(RelevantBooksList);
