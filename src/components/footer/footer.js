import React from "react";
import "./footer.scss";

const Footer = () => (
    <footer className="footer">
        <h2 className="footer__h2">BOOKSTORE</h2>
    </footer>
);

export default Footer;
