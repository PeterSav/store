import imgForNewBook from "../../assets/noImg.jpg";

export const isNum = input => /^\d+$/.test(input);

// ? I should have a correct regex here but I didn't have enough time to make
// ? or find one that works perfectly
export const isTitleValid = title => {
    if (title.length > 120) {
        return false;
    }
    const regexForTitle = /^[a-z0-9]+$/i;
    if (regexForTitle.test(title)) {
        if (title.length >= 10) {
            return true;
        }
    }
};

export const capitalizeFirstLetter = string =>
    string.charAt(0).toUpperCase() + string.substring(1);

export const numberOfSpaces = string => string.split(" ").length - 1;

export const hasMoreThanFourSpaces = string => {
    if (numberOfSpaces(string) > 4) {
        return true;
    }
};

export const hasLessThanXSpaces = (string, x) => {
    if (numberOfSpaces(string) < x) {
        return true;
    }
};

export const isYearValid = input => {
    if (isNum(input) && input.length === 4) {
        return true;
    }
};

export const isNumberOfPagesValid = input => {
    if (parseInt(input) <= 9999) {
        return true;
    }
};

// ? test for if the input that the user added is valid to be added
export const isValueValidForUpdate = (input, value) => {
    if (input === "title") {
        const regexForTitle = /^[a-z0-9]+$/i;
        if (regexForTitle.test(value)) {
            if (value.length <= 120) {
                return true;
            }
        }
    } else if (input === "description") {
        if (value.length <= 512) {
            return true;
        }
    } else if (input === "categories") {
        if (hasLessThanXSpaces(value, 4)) {
            return true;
        }
    } else if (input === "author") {
        if (hasLessThanXSpaces(value, 3)) {
            return true;
        }
    } else if (input === "publisher") {
        if (value.length <= 60) {
            return true;
        }
    } else if (input === "year") {
        if (isNum(value)) {
            if (value.length <= 4) {
                return true;
            }
        }
    } else if (input === "pages") {
        if (isNum(value)) {
            if (parseInt(value) <= 9999) {
                return true;
            }
        }
    } else if (input === "isbn") {
        if (isNum(value)) {
            if (value.length <= 13) {
                return true;
            }
        }
    } else if (input === "isbn10") {
        if (isNum(value)) {
            if (value.length <= 10) {
                return true;
            }
        }
    }

    if (value === "") {
        return true;
    }
};

// ! takes the bookToAdd from state and finds if there are error fields to update them in the state
export const errorHandler = bookToAdd => {
    const {
        title,
        description,
        categories,
        author,
        publisher,
        year,
        pages,
        isbn10,
        isbn,
    } = bookToAdd;
    let titleError = !isTitleValid(title);
    let descriptionError = description.length > 0 ? false : true;
    let categoriesError = categories.length > 0 ? false : true;
    let authorError = author.length > 0 ? false : true;
    let publisherError = publisher.length > 5 ? false : true;
    let yearError = year.length === 4 ? false : true;
    let pagesError = pages.length > 0 ? false : true;
    let isbn10Error = isbn10.length === 10 ? false : true;
    let isbnError = isbn.length === 13 ? false : true;
    return {
        title: titleError,
        description: descriptionError,
        categories: categoriesError,
        author: authorError,
        publisher: publisherError,
        year: yearError,
        pages: pagesError,
        isbn10: isbn10Error,
        isbn: isbnError,
    };
};

export const bookToAddCreator = (bookToAdd, id) => {
    const {
        title,
        description,
        categories,
        author,
        publisher,
        year,
        pages,
        isbn10,
        isbn,
    } = bookToAdd;

    return {
        title,
        description,
        categories,
        author,
        publisher,
        year,
        pages,
        isbn10,
        isbn,
        id,
        img: imgForNewBook,
        stars: 0,
    };
};
