import { map, mergeMap } from "rxjs/operators";
import { ofType } from "redux-observable";
import { combineEpics } from "redux-observable";
import {
    HANDLE_SEARCH_EPIC,
    HANDLE_SEARCH,
    HANDLE_ADD_BOOK_INPUTS_EPIC,
    HANDLE_ADD_BOOK_INPUTS_SUCCESS,
    HANDLE_ADD_BOOK_INPUTS_FAIL,
    NEW_BOOK_INITIALIZE,
    NEW_BOOK_INITIALIZE_SUCCESS,
    ADD_BOOK_BUTTON_PRESSED,
    ADD_BOOK_BUTTON_FAIL,
    ADD_BOOK_PROCESS,
    ADD_BOOK_PROCESS_SUCCESS,
    ADD_BOOK_PROCESS_FAIL,
    CREATE_RELEVANCE_LIST,
    CREATE_RELEVANCE_LIST_SUCCESS,
} from "./actions";
import cloneDeep from "lodash/cloneDeep";
import {
    isValueValidForUpdate,
    capitalizeFirstLetter,
    errorHandler,
    bookToAddCreator,
} from "../commons/helpers/helperFunctions";

import { bookToAddErrorsInit } from "./initializations";

// ? although it works kind of fine it should have some kind of lazy search algorithm
const handleSearchChange = (action$, state$) =>
    action$.pipe(
        ofType(HANDLE_SEARCH_EPIC),
        map(action => {
            const { payload } = action;
            const newSortedBooksList = [];
            state$.value.booksList.forEach(book => {
                if (book.title.toLowerCase().includes(payload.toLowerCase())) {
                    newSortedBooksList.push(book);
                } else if (
                    book.categories
                        .toLowerCase()
                        .includes(payload.toLowerCase())
                ) {
                    newSortedBooksList.push(book);
                } else if (book.year.includes(payload)) {
                    newSortedBooksList.push(book);
                } else if (
                    book.publisher.toLowerCase().includes(payload.toLowerCase())
                ) {
                    newSortedBooksList.push(book);
                }
            });
            return {
                type: HANDLE_SEARCH,
                payload,
                newSortedBooksList,
            };
        })
    );

const handleInputFromAddBook = (action$, state$) =>
    action$.pipe(
        ofType(HANDLE_ADD_BOOK_INPUTS_EPIC),
        map(action => {
            const { input, value } = action.payload;
            const bookToAdd = cloneDeep(state$.value.bookToAdd);
            bookToAdd[input] =
                input === "description" ? capitalizeFirstLetter(value) : value;
            if (isValueValidForUpdate(input, value)) {
                return {
                    type: HANDLE_ADD_BOOK_INPUTS_SUCCESS,
                    payload: bookToAdd,
                };
            } else {
                return { type: HANDLE_ADD_BOOK_INPUTS_FAIL };
            }
        })
    );

const addBookButtonPressed = (action$, state$) =>
    action$.pipe(
        ofType(ADD_BOOK_BUTTON_PRESSED),
        mergeMap(() => {
            const bookErrors = errorHandler(state$.value.bookToAdd);
            const allFalse = objectToCheckValue => objectToCheckValue === false;
            const hasErrors = !Object.values(bookErrors).every(allFalse);
            if (hasErrors) {
                return [
                    {
                        type: ADD_BOOK_BUTTON_FAIL,
                        payload: bookErrors,
                    },
                ];
            } else {
                return [{ type: ADD_BOOK_PROCESS }];
            }
        })
    );

const addBookButtonProcess = (action$, state$) =>
    action$.pipe(
        ofType(ADD_BOOK_PROCESS),
        mergeMap(() => {
            const newBooksList = cloneDeep(state$.value.booksList);
            const newId = newBooksList.length;
            let bookAlreadyExists;
            state$.value.booksList.forEach(book => {
                if (book.isbn === state$.value.bookToAdd.isbn) {
                    bookAlreadyExists = true;
                }
            });
            if (!bookAlreadyExists) {
                newBooksList.push(
                    bookToAddCreator(state$.value.bookToAdd, newId)
                );
                return [
                    {
                        type: ADD_BOOK_PROCESS_SUCCESS,
                        payload: { newBooksList, id: newId },
                    },
                    {
                        type: NEW_BOOK_INITIALIZE,
                    },
                ];
            } else {
                return [
                    {
                        type: ADD_BOOK_PROCESS_FAIL,
                    },
                ];
            }
        })
    );

const newBookInitialize = action$ =>
    action$.pipe(
        ofType(NEW_BOOK_INITIALIZE),
        map(() => {
            const bookToAdd = bookToAddErrorsInit;
            return {
                type: NEW_BOOK_INITIALIZE_SUCCESS,
                payload: bookToAdd,
            };
        })
    );

const createRelevanceList = (action$, state$) =>
    action$.pipe(
        ofType(CREATE_RELEVANCE_LIST),
        map(action => {
            const { payload } = action;
            // ? here a lazy search for certain keywords in title and description should be implemented and categories split and test
            const booksList = state$.value.booksList.map(book => {
                let counter = 0;
                if (book.year === payload.year) {
                    counter++;
                }
                if (book.publisher === payload.publisher) {
                    counter++;
                }
                if (book.author === payload.author) {
                    counter++;
                }
                return {
                    ...book,
                    relevance: counter,
                };
            });
            const relevantList = booksList
                .filter(book => book.id !== payload.id)
                .sort((a, b) => a.relevance - b.relevance)
                .reverse();
            return {
                type: CREATE_RELEVANCE_LIST_SUCCESS,
                payload: relevantList,
            };
        })
    );

export const rootEpic = combineEpics(
    handleSearchChange,
    handleInputFromAddBook,
    newBookInitialize,
    addBookButtonPressed,
    addBookButtonProcess,
    createRelevanceList
);
