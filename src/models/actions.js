export const SEARCH_INITIALIZE = "SEARCH_INITIALIZE";
export const HANDLE_SEARCH_EPIC = "HANDLE_SEARCH_EPIC";
export const HANDLE_SEARCH = "HANDLE_SEARCH";
export const HANDLE_ADD_BOOK_INPUTS_EPIC = "HANDLE_ADD_BOOK_INPUTS_EPIC";
export const HANDLE_ADD_BOOK_INPUTS_FAIL = "HANDLE_ADD_BOOK_INPUTS_FAIL";
export const HANDLE_ADD_BOOK_INPUTS_SUCCESS = "HANDLE_ADD_BOOK_INPUTS_SUCCESS";
export const NEW_BOOK_INITIALIZE = "NEW_BOOK_INITIALIZE";
export const NEW_BOOK_INITIALIZE_SUCCESS = "NEW_BOOK_INITIALIZE_SUCCESS";
export const ADD_BOOK_BUTTON_PRESSED = "ADD_BOOK_BUTTON_PRESSED";
export const ADD_BOOK_BUTTON_SUCCESS = "ADD_BOOK_BUTTON_SUCCESS";
export const ADD_BOOK_BUTTON_FAIL = "ADD_BOOK_BUTTON_FAIL";
export const ADD_BOOK_PROCESS = "ADD_BOOK_PROCESS";
export const ADD_BOOK_PROCESS_SUCCESS = "ADD_BOOK_PROCESS_SUCCESS";
export const ADD_BOOK_PROCESS_FAIL = "ADD_BOOK_PROCESS_FAIL";
export const CREATE_RELEVANCE_LIST = "CREATE_RELEVANCE_LIST";
export const CREATE_RELEVANCE_LIST_SUCCESS = "CREATE_RELEVANCE_LIST_SUCCESS";

export const searchInitialize = () => ({
    type: SEARCH_INITIALIZE,
});

export const handleSearchEpic = value => ({
    type: HANDLE_SEARCH_EPIC,
    payload: value,
});

export const handleAddBookInputs = (input, value) => ({
    type: HANDLE_ADD_BOOK_INPUTS_EPIC,
    payload: { input, value },
});

export const addBookButtonPressed = () => ({
    type: ADD_BOOK_BUTTON_PRESSED,
});

export const createRelevanceList = book => ({
    type: CREATE_RELEVANCE_LIST,
    payload: book,
});

export const newBookInitialize = () => ({ type: NEW_BOOK_INITIALIZE });
