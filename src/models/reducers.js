import {
    SEARCH_INITIALIZE,
    HANDLE_SEARCH,
    HANDLE_ADD_BOOK_INPUTS_SUCCESS,
    NEW_BOOK_INITIALIZE_SUCCESS,
    ADD_BOOK_BUTTON_FAIL,
    ADD_BOOK_PROCESS_SUCCESS,
    CREATE_RELEVANCE_LIST_SUCCESS,
} from "./actions";
import books from "../commons/initialBooks";
import noImage from "../assets/noImg.jpg";
import { bookToAddErrorsInit, bookToAddInit } from "./initializations";

// ? should have some extra data than those given so I create them in the initial state.
// ? id should be hashed somehow but I must have it to be able to use it on searches
const booksList = books.map((book, id) => {
    let dateOfBook = book.published.split("-");
    const year = dateOfBook[0];
    return {
        ...book,
        img: noImage,
        stars: Math.floor(((book.pages / (id + 1)) % 5) + 1),
        id,
        year,
        categories: "sci-fi",
        isbn10: book.isbn.substring(0, 10),
    };
});

const initialState = {
    booksList,
    search: "",
    sortedList: [],
    bookToAdd: bookToAddInit,
    showErrors: false,
    bookToAddErrors: bookToAddErrorsInit,
    showSaveBookButton: false,
    idOfNewBook: 0,
    relevantToBookList: [],
};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEARCH_INITIALIZE:
            return {
                ...state,
                search: "",
            };
        case HANDLE_SEARCH:
            return {
                ...state,
                search: action.payload,
                sortedList: action.newSortedBooksList,
            };
        case NEW_BOOK_INITIALIZE_SUCCESS:
            return {
                ...state,
                bookToAdd: action.payload,
            };
        case HANDLE_ADD_BOOK_INPUTS_SUCCESS:
            return {
                ...state,
                bookToAdd: action.payload,
                showErrors: false,
                showSaveBookButton: false,
            };
        case ADD_BOOK_BUTTON_FAIL:
            return {
                ...state,
                bookToAddErrors: action.payload,
                showErrors: true,
            };
        case ADD_BOOK_PROCESS_SUCCESS:
            return {
                ...state,
                showSaveBookButton: true,
                booksList: action.payload.newBooksList,
                idOfNewBook: action.payload.id,
            };
        case CREATE_RELEVANCE_LIST_SUCCESS:
            return {
                ...state,
                relevantToBookList: action.payload,
            };
        default:
            return state;
    }
};

export default rootReducer;
