export const bookToAddErrorsInit = {
    title: "",
    description: "",
    categories: "",
    author: "",
    publisher: "",
    year: "",
    pages: "",
    isbn10: "",
    isbn: "",
};

export const bookToAddInit = {
    title: "",
    description: "",
    categories: "",
    author: "",
    publisher: "",
    year: "",
    pages: "",
    isbn10: "",
    isbn: "",
};
