import React from "react";
import "./App.scss";
import { createEpicMiddleware } from "redux-observable";
import { Provider } from "react-redux";
import rootReducer from "./models/reducers";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { rootEpic } from "./models/epics";
import Home from "./components/home/index";
import Footer from "./components/footer/index";

const epicMiddleware = createEpicMiddleware();

const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(epicMiddleware))
);
epicMiddleware.run(rootEpic);

const App = () => (
    <Provider store={store}>
        <div className="wrapper">
            <Home />
        </div>
        <div className="push"></div>
        <Footer />
    </Provider>
);

export default App;
